function validate(columnInput, rowInput)
{
    if(isNaN(rowInput) || !Number.isInteger(parseFloat(rowInput)) || parseInt(rowInput) <= 0 ) 
    {   
        alert("Row value should be positive integer > 0");    
        return false;
    }

    if(isNaN(columnInput) || !Number.isInteger(parseFloat(columnInput)) || parseInt(columnInput) <= 0 ) 
    {   
        alert("Column value should be positive integer > 0");    
        return false;
    }
    
    return true;
}

function tableCreate() 
{   
    let columnInput = document.getElementById('columnsNumber').value;
    let rowInput = document.getElementById('rowsNumber').value;

    if(!validate(columnInput, rowInput))
        return;

    var columns = parseInt(columnInput);
    var rows = parseInt(rowInput);
    var body = document.getElementsByTagName('body')[0];
    var tbl = document.createElement('table');
    tbl.setAttribute('border', '');
    tbl.addEventListener("click", function(e) {tableIsClicked(e)});
    
    for (var i = 0; i < rows; i++) 
    {
        var tr = tbl.insertRow(i);
        for (var j = 0; j < columns; j++) 
        {
                var td = tr.insertCell(j);
                td.textContent = (i + 1).toString() + (j + 1);
        }
    }
    
    body.getElementsByTagName('table').length ? body.replaceChild(tbl, body.getElementsByTagName('table')[0]) : body.appendChild(tbl);                                                                        
}

function tableIsClicked(e)
{       
    if(e.target.nodeName == "TD")
    {   
        let targetStyle = getComputedStyle(e.target, null);
        let targetHeight = parseFloat(targetStyle.getPropertyValue('height').replace("px", ""));
        let targetWidth = parseFloat(targetStyle.getPropertyValue('width').replace("px", ""));
        let targetPadding = parseFloat(targetStyle.getPropertyValue('padding').replace("px", ""));
        let botBorder = targetHeight + 2 * targetPadding;
        let rightBorder = targetWidth + 2 * targetPadding;
        
        if(e.offsetX < 0 || e.offsetY < 0 || e.offsetX > rightBorder || e.offsetY > botBorder)
        {
            alert("Border is clicked");
            if(e.target.style.borderColor != "cornflowerblue")
                e.target.style.borderColor = "cornflowerblue";
            else
                e.target.style.borderColor = "rgb(128, 128, 128)";
        }
        else
        {
            if(e.target.style.backgroundColor != "red")
                e.target.style.backgroundColor = "red";
            else
                e.target.style.backgroundColor = "cornflowerblue";
        }
    }
}